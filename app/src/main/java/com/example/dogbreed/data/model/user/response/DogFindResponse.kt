package com.example.dogbreed.data.model.user.response


import com.google.gson.annotations.SerializedName

data class DogFindResponse(
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("message")
    val message: List<String> = emptyList()
)