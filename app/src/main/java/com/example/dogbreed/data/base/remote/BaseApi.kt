package com.example.dogbreed.data.base.remote

import com.example.dogbreed.data.model.user.response.DogFindResponse
import com.example.dogbreed.util.API_FIND_DOG_IMAGES
import retrofit2.http.GET
import retrofit2.http.Path

interface BaseApi {
    @GET(API_FIND_DOG_IMAGES)
    suspend fun getImagesByDogName(
        @Path("name") name: String
    ): DogFindResponse
}