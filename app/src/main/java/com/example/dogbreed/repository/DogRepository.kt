package com.example.dogbreed.repository

import com.example.dogbreed.data.base.remote.BaseApi
import com.example.dogbreed.data.model.user.response.DogFindResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

open class DogRepository(private val baseApi: BaseApi) {

    suspend fun getImagesByDogName(name: String): Flow<DogFindResponse> {
        return flow {
            emit(baseApi.getImagesByDogName(name))
        }.flowOn(Dispatchers.IO)
    }
}