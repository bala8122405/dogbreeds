package com.example.dogbreed.util

import android.content.Context
import android.net.ConnectivityManager
import com.example.dogbreed.DogApplication

fun isNetworkAvailable(): Boolean {
    val connectivityManager =
        DogApplication.ctx?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activateNetworkInfo = connectivityManager.activeNetworkInfo
    return activateNetworkInfo != null && activateNetworkInfo.isConnected
}