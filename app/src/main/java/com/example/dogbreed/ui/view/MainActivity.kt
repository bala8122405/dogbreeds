package com.example.dogbreed.ui.view

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.dogbreed.R
import com.example.dogbreed.databinding.ActivityMainBinding
import com.example.dogbreed.ui.adapter.ImageAdapter
import com.example.dogbreed.ui.viewmodel.DogViewModel
import com.example.dogbreed.util.hideKeyboard
import kotlinx.coroutines.flow.collectLatest

class MainActivity : AppCompatActivity() {
    private lateinit var dogViewModel: DogViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViews()
        initObservers()
    }

    private fun initObservers() {
        lifecycleScope.launchWhenCreated {
            dogViewModel.imageListSharedFlow.collectLatest { data ->
                val adapter = ImageAdapter(data.message)
                binding.rvList.adapter = adapter
                if (data.message.isEmpty()) {
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.no_data_found),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        lifecycleScope.launchWhenCreated {
            dogViewModel.errorSharedFlow.collectLatest {
                Toast.makeText(this@MainActivity, "" + it, Toast.LENGTH_SHORT).show()
            }
        }

        lifecycleScope.launchWhenCreated {
            dogViewModel.loadingSharedFlow.collectLatest {
                if (it) {
                    binding.progressBar.visibility = View.VISIBLE
                } else {
                    binding.progressBar.visibility = View.GONE
                }
            }
        }
    }

    private fun initViews() {
        dogViewModel = ViewModelProvider(this)[DogViewModel::class.java]
        binding.btnFind.setOnClickListener {
            if (!TextUtils.isEmpty(binding.edtName.text.toString().trim())) {
                dogViewModel.getImagesByDogName(binding.edtName.text.toString().trim())
                it.hideKeyboard()
            } else
                Toast.makeText(
                    this@MainActivity,
                    getString(R.string.enter_dog_name),
                    Toast.LENGTH_SHORT
                )
                    .show()
        }
        binding.rvList.layoutManager = GridLayoutManager(this, 2)
    }
}