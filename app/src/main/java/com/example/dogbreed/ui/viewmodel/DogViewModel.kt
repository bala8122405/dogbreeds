package com.example.dogbreed.ui.viewmodel

import androidx.lifecycle.viewModelScope
import com.example.dogbreed.data.base.remote.BaseService
import com.example.dogbreed.data.model.user.response.DogFindResponse
import com.example.dogbreed.repository.DogRepository
import com.example.dogbreed.util.isNetworkAvailable
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch

class DogViewModel : BaseObservableViewModel() {

    private val dogRepository = BaseService().getBaseApi()?.let { DogRepository(it) }

    private val _imageListSharedFlow = MutableSharedFlow<DogFindResponse>()
    val imageListSharedFlow = _imageListSharedFlow as SharedFlow<DogFindResponse>

    private val _loadingSharedFlow = MutableSharedFlow<Boolean>()
    val loadingSharedFlow = _loadingSharedFlow as SharedFlow<Boolean>

    private val _errorSharedFlow = MutableSharedFlow<String>()
    val errorSharedFlow = _errorSharedFlow as SharedFlow<String>

    fun getImagesByDogName(dogName: String) {
        viewModelScope.launch {
            if (isNetworkAvailable()) {
                _loadingSharedFlow.emit(true)
                dogRepository?.getImagesByDogName(dogName)?.catch {
                    _loadingSharedFlow.emit(false)
                    _errorSharedFlow.emit(it.message.toString())

                }?.collect {
                    _loadingSharedFlow.emit(false)
                    _imageListSharedFlow.emit(it)
                }
            } else {
                _errorSharedFlow.emit("Please check the Internet connection and try again.")
            }
        }
    }
}