package com.example.dogbreed.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.dogbreed.data.base.remote.BaseService
import com.example.dogbreed.repository.DogRepository
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

open class DogViewModelTest {
    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    var dogRepository: DogRepository? = null

    @Before
    fun setupNYCSchoolViewModel() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.openMocks(this)

        // Get a reference to the class under test
        dogRepository = BaseService().getBaseApi()?.let { DogRepository(it) }
    }

    @Test
    fun `Fetch Dog Breed list and having data`() {
        runBlocking {
            dogRepository?.getImagesByDogName("lhasa")?.collectLatest {
                Assert.assertFalse(it.message.isEmpty())
            }
        }
    }

    @Test
    fun `Fetch Dog Breed list and no data`() {
        runBlocking {
            dogRepository?.getImagesByDogName("test")?.collectLatest {
                Assert.assertTrue(it.message.isEmpty())
            }
        }
    }
}